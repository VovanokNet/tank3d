﻿using System;
using UnityEngine;

public class ProjectileInfluence : MonoBehaviour
{
	public float Radius { get; private set; }

	private float curRadius;

	private int stage;

	private float timeExpansion = 1f;
	private float timeStay = 1f;

	private float curStageTime;

	public event EventHandler<ActionEventArgs> OnPeak;
	public event EventHandler<ActionEventArgs> OnEnd;

	public void Init(Vector3 position, float radius)
	{
		transform.position = position;
		Radius = radius;
	}

	private void Update()
	{
		curStageTime += Time.deltaTime;

		if( stage == 0 )
		{
			curRadius = Mathf.Lerp( 0f, Radius, curStageTime / timeExpansion );
			if( curRadius >= Radius )
			{
				stage++;
				curStageTime = 0;

				if( OnPeak != null )
				{
					OnPeak(this, new ActionEventArgs(this));
				}
			}
		}

		if( stage == 1 )
		{
			if( curStageTime >= timeStay )
			{
				stage++;
				curStageTime = 0;
			}
		}

		if( stage == 2 )
		{
			curRadius = Mathf.Lerp( Radius, 0f, curStageTime / timeExpansion );
			if( curRadius <= 0f )
			{
				stage++;
				curStageTime = 0;
				if( OnEnd != null )
				{
					OnEnd(this, new ActionEventArgs(this));
				}
			}
		}

		if( stage == 3 )
		{
			Destroy( gameObject );
		}

		transform.localScale = new Vector3( curRadius, curRadius, curRadius );
	}

	public class ActionEventArgs : EventArgs
	{
		public ProjectileInfluence ProjectileInfluence { get; private set; }

		public ActionEventArgs(ProjectileInfluence projectileInfluence)
		{
			ProjectileInfluence = projectileInfluence;
		}
	}
}
