﻿using UnityEngine;

public class ProjectileTracer : MonoBehaviour
{
	[SerializeField] private float MinDistanceForDrawTrace = 0.03f;

	public Vector3 LastPoint { get; private set; }

	private int pointsCount;

	private LineRenderer traceView;
	private LineRenderer TraceView
	{
		get
		{
			if (traceView == null)
			{
				traceView = GetComponent<LineRenderer>();
			}

			return traceView;
		}
	}

	public void AddPoint(Vector3 pointPosition)
	{
		if (pointsCount > 0 && Vector3.Distance(LastPoint, pointPosition) < MinDistanceForDrawTrace)
			return;

		pointsCount++;
		TraceView.SetVertexCount(pointsCount);
		TraceView.SetPosition(pointsCount - 1, pointPosition);
		LastPoint = pointPosition;
	}

	public void Reset()
	{
		TraceView.SetVertexCount(0);
		pointsCount = 0;
	}
}
