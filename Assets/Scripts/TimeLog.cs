﻿using System;
using UnityEngine;

public class TimeLog : IDisposable
{
	private DateTime startTime;
	private string meterName;
	private bool isError;

	public TimeLog(string meterName, bool isError = false)
	{
		startTime = DateTime.Now;
		this.meterName = meterName ?? "Time meter";
		this.isError = isError;
	}

	public void Dispose()
	{
		var message = string.Format("{0} : {1}", meterName, DateTime.Now - startTime);

		if (!isError)
		{
			Debug.Log(message);
		}
		else
		{
			Debug.LogError(message);
		}
	}
}
