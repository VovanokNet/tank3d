﻿using System;
using UnityEngine;

public class Projectile : MonoBehaviour
{
	public event EventHandler<InfluenceEventArgs> OnInfluencePeak;
	public event EventHandler<InfluenceEventArgs> OnInfluenceEnd;

	private Vector3 previousPosition;

	public void Init(Vector3 src, Vector3 dir, float force)
	{
		transform.position = src;
		previousPosition = transform.position;

		GetComponent<Rigidbody>().AddForce(dir //Vector3.ClampMagnitude(dir, force)
			, ForceMode.Impulse);

		GameManager.Instance.ProjectileTracer.Reset();
		GameManager.Instance.ProjectileTracer.AddPoint(src);
	}

	private void Update()
	{
		GameManager.Instance.ProjectileTracer.AddPoint(transform.position);

		if (previousPosition == transform.position)
			return;

		Debug.DrawLine(previousPosition, transform.position, Color.white);

		Vector3 hitPosition;
		if (GameManager.Instance.Terrain.Linecast(previousPosition, transform.position, out hitPosition))
		{
			gameObject.SetActive(false);

			var influence = InstantiateProjectileInfluence();
			influence.Init(transform.position, Player.BOOM_RADIUS);
			influence.OnPeak += OnInnerInfluencePeak;
			influence.OnEnd += OnInnerInfluenceEnd;
		}

		previousPosition = transform.position;
	}

	private ProjectileInfluence InstantiateProjectileInfluence()
	{
		var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		go.name = "Influence";
		go.GetComponent<Renderer>().material.color = Color.red;
		go.transform.localScale = Vector3.zero;

		return go.AddComponent<ProjectileInfluence>();
	}

	private void OnInnerInfluencePeak(object sender, ProjectileInfluence.ActionEventArgs args)
	{
		args.ProjectileInfluence.OnPeak -= OnInnerInfluencePeak;

		if (OnInfluencePeak != null)
		{
			OnInfluencePeak(this, new InfluenceEventArgs(this, args.ProjectileInfluence));
		}
	}

	private void OnInnerInfluenceEnd(object sender, ProjectileInfluence.ActionEventArgs args)
	{
		args.ProjectileInfluence.OnEnd -= OnInnerInfluenceEnd;

		Destroy(gameObject);

		if (OnInfluenceEnd != null)
		{
			OnInfluenceEnd(this, new InfluenceEventArgs(this, args.ProjectileInfluence));
		}
	}

	public class InfluenceEventArgs : EventArgs
	{
		public Projectile Projectile { get; private set; }
		public ProjectileInfluence ProjectileInfluence { get; private set; }

		public InfluenceEventArgs(Projectile projectile, ProjectileInfluence projectileInfluence)
		{
			Projectile = projectile;
			ProjectileInfluence = projectileInfluence;
		}
	}
}
