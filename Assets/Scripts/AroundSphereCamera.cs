﻿using System;
using UnityEngine;

public class AroundSphereCamera : MonoBehaviour
{
	[SerializeField] private float distanceCameraToCenter = 3;
	[SerializeField] private float zoomStep = 0.1f;
	[SerializeField] private float minDistanceFromCenter = 1.2f;
	[SerializeField] private float maxDistanceFromCenter = 5f;

	private const float RotationFactor = 0.2f;
	private const string LeftButtonName = "Fire1";
	private const string RightButtonName = "Fire2";
	private const float PositionXbounder = 89;

	private Vector2 rotation;

	private bool isRotating;
	private Vector3 dragPosition;
	private static Vector3 initialCameraPosition;
	private bool isNeedReposition;

	private void Start()
	{
		isNeedReposition = true;
	}

	private void Update()
	{
		if( Input.GetButtonDown( RightButtonName ) || Input.GetButtonDown( LeftButtonName ) )
		{
			isRotating = true;
			dragPosition = Input.mousePosition;
		}

		if( Input.GetButtonUp( RightButtonName ) || Input.GetButtonUp( LeftButtonName ) )
		{
			isRotating = false;
		}

		if( isRotating )
		{
			ChangeRotation(new Vector2(
					-RotationFactor * (dragPosition.y - Input.mousePosition.y),
					-RotationFactor * (dragPosition.x - Input.mousePosition.x)));

			dragPosition = Input.mousePosition;
		}

		var deltaWheel = Input.GetAxis("Mouse ScrollWheel");
		if (deltaWheel > 0)
		{
			Zoom(zoomStep);
		}
		else if (deltaWheel < 0)
		{
			Zoom(-zoomStep);
		}

		if (isNeedReposition)
		{
			Reposition();
		}
	}

	private void ChangeRotation( Vector2 deltaAngles )
	{
		rotation.x = Mathf.Clamp(rotation.x + deltaAngles.x, -PositionXbounder, PositionXbounder);
		rotation.y = rotation.y + deltaAngles.y;

		isNeedReposition = true;
	}

	private void Reposition()
	{
		isNeedReposition = false;

		initialCameraPosition.z = distanceCameraToCenter;
		transform.position = Quaternion.Euler( rotation.x, rotation.y, 0 ) * initialCameraPosition;
		transform.LookAt( Vector3.zero );
	}

	private void Zoom(float delta)
	{
		distanceCameraToCenter = Mathf.Clamp(distanceCameraToCenter + delta, minDistanceFromCenter, maxDistanceFromCenter);

		isNeedReposition = true;
	}

	#region EventArgs

	public class ColliderClickEventArgs : EventArgs
	{
		public RaycastHit Hit { get; private set; }

		public ColliderClickEventArgs( RaycastHit hit )
		{
			Hit = hit;
		}
	}

	#endregion
}