﻿using System;
using UnityEngine;

public class Player : MonoBehaviour
{
	[SerializeField] private GameObject projectilePrefab;
	[SerializeField] private Transform shootSrcPivot;

	//TODO: 1. Сделать пивот для запуска снаряда
	//TODO: 2. Вычисления ограничений по каждой компоненте для вектора запуска снаряда
	public const float BOOM_RADIUS = 0.2f;
	public const float SHOOT_DIR_VECTOR_LENGTH = 1f;

	public string Name { get; private set; }
	public Color Color { get; private set; }

	private int hp;
	public int Hp {
		get { return hp; }
		set { hp = Mathf.Clamp(value, 0, 100); }
	}
	
	public float ShootDirAngleAlong { get; set; }

	public float ShootDirAngleAcross { get; set; }

	public bool IsDied
	{
		get { return Hp <= 0; }
	}

	public event EventHandler<ShootEventArgs> OnShoot;
	public event EventHandler<EndStepEventArgs> OnEndStep;

	private Vector3 ShootDir
	{
		get
		{
			var result = Quaternion.Euler(ShootDirAngleAcross, ShootDirAngleAlong, 0f) * (-transform.forward);

			var scaleFactor = SHOOT_DIR_VECTOR_LENGTH / result.magnitude;
			result.Scale(new Vector3(scaleFactor, scaleFactor, scaleFactor));

			return result;
		}
	}

	public void Init(string playerName, Color playerColor, Vector3 playerPositionDir)
	{
		Name = playerName;
		gameObject.name = playerName;

		Color = playerColor;

		var renderers = GetComponentsInChildren<Renderer>();
		foreach (var curRenderer in renderers)
		{
			curRenderer.material.color = playerColor;
		}
		
		Hp = 100;

		GetComponent<ObjectOnEarth>().Init(playerPositionDir);
	}

	public void Shoot()
	{
		var projectile = Utils.InstantiateByPrefab<Projectile>(projectilePrefab);
		projectile.Init(shootSrcPivot.position, ShootDir, 20f);
		projectile.OnInfluenceEnd += OnInfluenceEndProjectile;

		if (OnShoot != null)
		{
			OnShoot(this, new ShootEventArgs(this, projectile));
		}
	}

	private void OnInfluenceEndProjectile(object sender, Projectile.InfluenceEventArgs args)
	{
		args.Projectile.OnInfluenceEnd -= OnInfluenceEndProjectile;

		if (OnEndStep != null)
		{
			OnEndStep(this, new EndStepEventArgs(this));
		}
	}

	public class ShootEventArgs : EventArgs
	{
		public Player Player { get; private set; }
		public Projectile Projectile { get; private set; }

		public ShootEventArgs(Player player, Projectile projectile)
		{
			Player = player;
			Projectile = projectile;
		}
	}

	public class EndStepEventArgs : EventArgs
	{
		public Player Player { get; private set; }

		public EndStepEventArgs(Player player)
		{
			Player = player;
		}
	}
}