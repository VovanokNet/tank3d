﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class GameManager : MonoBehaviour
{
	[SerializeField] private ShootDirectionPointer shootDirectionPointer;
	[SerializeField] private GameObject playerPrefab;
	[SerializeField] private GameObject projectilePrefab;
	[SerializeField] private GameObject projectileTracerPrefab;

	public SphereTerrain Terrain { get; private set; }

	public Player CurrentPlayer { get; private set; }

	public ProjectileTracer ProjectileTracer { get; private set; }

	private List<Player> players = new List<Player>();
	private bool isActionsGuiLocked;

	public static GameManager Instance { get; private set; }

	private void Awake()
	{
		Instance = this;
		Instance.Init();

		ProjectileTracer = Utils.InstantiateByPrefab<ProjectileTracer>(projectileTracerPrefab);
		ProjectileTracer.Reset();
	}

	private void Init()
	{
		Terrain = FindObjectOfType<SphereTerrain>();
	}

	public Player AddPlayer(string playerName, Vector3 playerPositionDir, Color playerColor)
	{
		var newPlayer = Utils.InstantiateByPrefab<Player>(playerPrefab);
		newPlayer.Init(playerName, playerColor, playerPositionDir);
		newPlayer.OnShoot += OnPlayerShoot;
		newPlayer.OnEndStep += OnPlayerEndStep;

		if (players.Count == 0)
			CurrentPlayer = newPlayer;

		players.Add(newPlayer);
		return newPlayer;
	}

	public void ChangePlayer()
	{
		isActionsGuiLocked = false;

		if (CurrentPlayer == null)
		{
			CurrentPlayer = players.FirstOrDefault();
			return;
		}

		int curPlayerIndex = players.IndexOf(CurrentPlayer);
		if (curPlayerIndex >= players.Count - 1)
		{
			CurrentPlayer = players.FirstOrDefault();
			return;
		}

		CurrentPlayer = players[curPlayerIndex + 1];
	}

	private void OnGUI()
	{
		//Имя текущего игрока
		if (CurrentPlayer != null)
		{
			GUI.Label(new Rect(5, 5, 100, 40), CurrentPlayer.name);
		}

		//Жизни всех игроков
		for (int i = 0; i < players.Count; i++)
		{
			GUI.Label(new Rect(5, 40 + i*25, 50, 25), GetPlayerLabel(i),
				new GUIStyle {normal = new GUIStyleState {textColor = players[i].Color}});
		}

		var yPos = 40 + players.Count * 40;

		//Направление и кнопка запуска выстрела
		if (CurrentPlayer != null)
		{
			if (!isActionsGuiLocked)
			{
				GUI.Label(new Rect(5, yPos, 100, 40), string.Format("Along   = {0}", CurrentPlayer.ShootDirAngleAlong));
				CurrentPlayer.ShootDirAngleAlong = Mathf.Floor(GUI.HorizontalSlider(new Rect(100, yPos + 5, 100, 40),
					CurrentPlayer.ShootDirAngleAlong, -90, 90));

				yPos += 40;

				GUI.Label(new Rect(5, yPos, 100, 40), string.Format("Across = {0}", CurrentPlayer.ShootDirAngleAcross));
				CurrentPlayer.ShootDirAngleAcross = Mathf.Floor(GUI.HorizontalSlider(new Rect(100, yPos + 5, 100, 40),
					CurrentPlayer.ShootDirAngleAcross, -90, 90));

				yPos += 40;

				if (GUI.Button(new Rect(5, yPos, 100, 40), "Fire"))
				{
					isActionsGuiLocked = true;
					CurrentPlayer.Shoot();
				}

				yPos += 40;
			}
		}

		//Кнопка перезагрузки
		if (GUI.Button(new Rect(5, yPos, 100, 40), "Reload"))
		{
			Application.LoadLevel("main");
		}
	}

	private void OnPlayerShoot(object sender, Player.ShootEventArgs args)
	{
		args.Projectile.OnInfluencePeak += OnPlayerProjectileInfluencePeak;
	}

	private void OnPlayerEndStep(object sender, Player.EndStepEventArgs args)
	{
		ChangePlayer();
	}
	
	private void OnDestroy()
	{
		foreach (var player in players)
		{
			player.OnShoot -= OnPlayerShoot;
			player.OnEndStep -= OnPlayerEndStep;
		}
	}

	private void OnPlayerProjectileInfluencePeak(object sender, Projectile.InfluenceEventArgs args)
	{
		args.Projectile.OnInfluencePeak -= OnPlayerProjectileInfluencePeak;

		foreach (var player in players)
		{
			if (Vector3.Distance(args.ProjectileInfluence.transform.position, player.transform.position) < Player.BOOM_RADIUS)
			{
				player.Hp -= 10;
			}
		}

		Terrain.SubstractSphere(args.ProjectileInfluence.transform.position, args.ProjectileInfluence.Radius);
	}

	private string GetPlayerLabel(int num)
	{
		var sb = new StringBuilder();

		//Текущий пользователь
		if (players[num] == CurrentPlayer)
		{
			sb.Append(">>> ");
		}

		//Постоянная часть
		sb.AppendFormat("P{0}: {1}", num + 1, players[num].Hp);

		//Ярлык смерти игрока
		if (players[num].IsDied)
		{
			sb.Append(" - DIED");
		}

		return sb.ToString();
	}
}