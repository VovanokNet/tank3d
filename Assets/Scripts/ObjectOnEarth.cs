﻿using UnityEngine;

public class ObjectOnEarth : MonoBehaviour
{
	private Rigidbody selfRigidbody;

	public Rigidbody SelfRigidbody
	{
		get
		{
			if (selfRigidbody == null)
			{
				selfRigidbody = GetComponent<Rigidbody>();
			}

			return selfRigidbody;
		}
	}

	public void Init(Vector3 positionDirection)
	{
		transform.position = positionDirection;
		transform.LookAt(Vector3.zero);
	}

	private void Update()
	{
		Vector3 g = Vector3.ClampMagnitude(-transform.position, 0.001f)*1000f*9.8f;

		SelfRigidbody.AddForce(SelfRigidbody.mass * g, ForceMode.Acceleration);
	}
}
