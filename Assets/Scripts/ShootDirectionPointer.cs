﻿using UnityEngine;

public class ShootDirectionPointer : MonoBehaviour
{
	[SerializeField] private float length = 0.1f;
	[SerializeField] private Color startColor = Color.cyan;
	[SerializeField] private Color endColor = Color.cyan;
	[SerializeField] private float startWidth = 0.01f;
	[SerializeField] private float endWidth = 0f;

	private LineRenderer lineRenderer;

	private void Start()
	{
		lineRenderer = GetComponent<LineRenderer>();

		if (lineRenderer == null)
		{
			Debug.LogError("ShootDirectionPointer.Start() : line renderer is not assigned");
			return;
		}

		lineRenderer.SetColors(startColor, endColor);
		lineRenderer.SetWidth(startWidth, endWidth);
		lineRenderer.SetVertexCount(2);
	}

	public void Reposition(Vector3 src, Vector3 dir)
	{
		lineRenderer.SetPosition(0, src);
		lineRenderer.SetPosition(1, src + Vector3.ClampMagnitude(dir, length));
	}
}