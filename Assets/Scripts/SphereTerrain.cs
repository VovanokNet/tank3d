﻿using System;
using UnityEngine;

using Random = UnityEngine.Random;

public class SphereTerrain : MonoBehaviour
{
	public const float NOT_DESTROYABLE_RADIUS = 0.1f;
	public const float TERRRAIN_AMPLITUDE = 0.1f;

	public event EventHandler<EventArgs> OnTransformate;

	private MeshCollider meshCollider;
	private MeshAdapter mesh;

	private void Start()
	{
		using (new TimeLog("Create mesh adapter", true))
		{
			mesh = new MeshAdapter(GetComponent<MeshFilter>().mesh);
		}

		using (new TimeLog("Generate terrain", true))
		{
			foreach (var point in mesh.Points)
			{
				var pointPolarPos = new PolarVector3(point.Position);
				float deltaRadius = 2f * TERRRAIN_AMPLITUDE * Mathf.PerlinNoise(pointPolarPos.Teta, pointPolarPos.Fi) - TERRRAIN_AMPLITUDE;
				pointPolarPos.R += deltaRadius;

				point.Position = pointPolarPos.DecardVector;

				//var heightFactor = Random.value > 0.1f ? 0.1f : 0.4f;
				//point.Position +=// point.Position +
				//                 Vector3.ClampMagnitude(point.Position - transform.position, 1f) * heightFactor * Random.value;
			}
		}

		using (new TimeLog("Flush", true))
		{
			mesh.Flush();
		}

		using (new TimeLog("Regenerate collider", true))
		{
			RegenerateCollider();
		}

		Transformate();
	}

	public void SubstractSphere(Vector3 position, float radius)
	{
		var boomGhost = new GameObject("BoomGhost");
		boomGhost.transform.position = position;
		var boomGhostCoolider = boomGhost.AddComponent<SphereCollider>();
		boomGhostCoolider.radius = radius;
		
		RaycastHit hit;
		foreach (var terrainPoint in mesh.Points)
		{
			var rayFromCore = new Ray(transform.position, terrainPoint.Position - transform.position);
			var distanceToCore = Vector3.Distance(terrainPoint.Position, transform.position);

			if (boomGhostCoolider.Raycast(rayFromCore, out hit, distanceToCore))
			{
				terrainPoint.Position = Vector3.Distance(hit.point, transform.position) <= NOT_DESTROYABLE_RADIUS
					? Vector3.ClampMagnitude(terrainPoint.Position, NOT_DESTROYABLE_RADIUS)
					: hit.point;
			}
		}

		Destroy(boomGhost);

		mesh.Flush();
		RegenerateCollider();
		Transformate();
	}

	public Vector3 GetPointOnSurface(Vector3 positionDir)
	{
		var rayToCore = new Ray(positionDir, transform.position - positionDir);
		var distanceToCore = Vector3.Distance(positionDir, transform.position);

		RaycastHit hit;
		if (meshCollider.Raycast(rayToCore, out hit, distanceToCore))
		{
			return hit.point;
		}
		
		return transform.position;
	}

	public float DistanceToSurface(Vector3 point)
	{
		return Vector3.Distance(point, GetPointOnSurface(point));
	}

	public bool Linecast(Vector3 start, Vector3 end, out Vector3 hitPosition)
	{
		hitPosition = Vector3.zero;
		RaycastHit hitInfo;
		if (Physics.Linecast(start, end, out hitInfo) && hitInfo.collider == meshCollider)
		{
			hitPosition = hitInfo.point;
			return true;
		}

		return false;
	}

	private void RegenerateCollider()
	{
		if (meshCollider != null)
		{
			Destroy(meshCollider);
		}

		meshCollider = gameObject.AddComponent<MeshCollider>();
	}

	private void Transformate()
	{
		if (OnTransformate != null)
		{
			OnTransformate(this, new EventArgs());
		}
	}
}