﻿using System.Runtime.InteropServices;
using UnityEngine;

public class GameEnvironment : MonoBehaviour
{
	[SerializeField] private GameManager gameManager;

	public void Start()
	{
		AddPlayer(1, Vector3.right);
		AddPlayer(2, Vector3.left);
		AddPlayer(3, Vector3.one);
		AddPlayer(4, Vector3.up);
		AddPlayer(5, Vector3.down);
	}

	private void AddPlayer(int num, Vector3 positionDir)
	{
		gameManager.AddPlayer(string.Format("Player{0}", num), GetCoorectPointToSpawn(positionDir), GetRandomColor());
	}

	private Color GetRandomColor()
	{
		return new Color(Random.value, Random.value, Random.value, 1.0f);
	}

	private Vector3 GetCoorectPointToSpawn(Vector3 positionDir)
	{
		var result = gameManager.Terrain.GetPointOnSurface(positionDir);
		result.Scale(new Vector3(1.1f, 1.1f, 1.1f));
		return result;
	}
}
