﻿using UnityEngine;

public static class Utils
{
	public static T InstantiateByPrefab<T>(GameObject prefab, GameObject container = null) where T: MonoBehaviour
	{
		var go = Object.Instantiate(prefab) as GameObject;
		if (go == null)
		{
			Debug.LogError("Instantiated game object is null");
			return null;
		}
		
		if (container != null)
			go.transform.parent = container.transform;

		var component = go.GetComponent<T>();
		if (component != null)
			return component;

		return go.AddComponent<T>();
	}
}
